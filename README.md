# feynman-diagram-template

## Dependencies

```shell
sudo dnf install texlive-collection-basic texlive-tikz-feynman texlive-tikz-feynhand texlive-pgfopts
```

## Compiling

Compile with
```shell
make article.pdf
```
or just
```shell
make
```
to display the pdf too.

## Reference

* [manual](http://texdoc.net/texmf-dist/doc/latex/tikz-feynhand/tikz-feynhand.userguide.pdf)
