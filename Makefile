DOCNAME=article
TEX=pdflatex

all: view

.PHONY: clean view wipe

$(DOCNAME).pdf: $(DOCNAME).tex diagrams.tex tikz/
	mkdir -p graphics/tikz
	$(TEX) -shell-escape $<

view: $(DOCNAME).pdf
	evince $< &

clean:
	-rm *.blg *.bbl *.aux *.log *.out *.toc *.fls *.nav *.snm *.fdb_latexmk *.synctex.gz *.auxlock

wipe: clean
	-rm *.pdf
	-rm -r graphics/
